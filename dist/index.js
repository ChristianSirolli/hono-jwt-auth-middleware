import { deleteCookie, getSignedCookie, setSignedCookie } from 'hono/cookie';
import { sign, verify, decode } from 'hono/jwt';
import { JwtAlgorithmNotImplemented, JwtTokenExpired, JwtTokenInvalid, JwtTokenIssuedAt, JwtTokenNotBefore, JwtTokenSignatureMismatched } from 'hono/utils/jwt/types';
import { HTTPException } from 'hono/http-exception';
import { randomUUID } from 'node:crypto';
import { Buffer } from 'node:buffer';
export class JWTAuth {
    domain;
    sessionLength;
    redirectURI;
    redirectStyle;
    logging;
    logger;
    cookie;
    jwt;
    sessionCache = {};
    /**
     * JWT Auth Session Manager
         *
     */
    constructor(config) {
        this.domain = config?.domain || 'localhost';
        this.cookie = {
            key: 'Authorization',
            secret: randomUUID(),
            httpOnly: true,
            secure: true,
            path: '/',
            sameSite: 'Strict',
            ...config?.cookie
        };
        this.jwt = {
            nbf: 5 * 60,
            secret: randomUUID(),
            alg: 'HS512',
            ...config?.jwt
        };
        this.sessionLength = config?.sessionLength || 12 * 60 * 60;
        this.redirectURI = config?.redirectURI || '/login';
        this.redirectStyle = config?.redirectStyle || 'referrer';
        this.logging = config?.logging != undefined ? config?.logging : false;
        this.logger = (str, ...rest) => (config && config.logger) ? config.logger(str ?? '', ...rest.filter(v => v != undefined)) : console.log(str, ...rest);
        this.log('JWTAuth initialized');
    }
    log(message, ...rest) {
        if (this.logging)
            this.logger(message, ...rest);
    }
    /**
     * Login Page Helper
     * @returns MiddlewareHandler
     */
    loginPageHelper() {
        return (async (c, next) => {
            const req = c.req;
            this.log(req.header('Referer'), req.raw.referrer, req.url);
            if ((await this.getSession(c))?.at(1)) {
                // user is logged in
                let uri;
                if (this.redirectStyle == 'referrer') {
                    uri = req.header('Referer') || req.raw.referrer;
                }
                else {
                    uri = atob(new URL(req.url).searchParams.get('r') ?? '');
                }
                this.log('Trying to redirect to', uri);
                if (uri == 'about:client' || uri == '' || !uri || req.url.match(uri)) {
                    uri = '/';
                }
                return c.redirect(uri);
            }
            else {
                // user is not logged in
                return next();
            }
        }); // as unknown as MiddlewareHandler
    }
    /**
     * Auth Handler for API Endpoints
     * @returns MiddlewareHandler
     */
    apiAuth(accessCheckHandler) {
        return (async (c, next) => {
            const req = c.req;
            const authResults = await this.jwtAuth(c);
            if (authResults.code == 200) {
                if (!accessCheckHandler) {
                    return next();
                }
                else if (accessCheckHandler(authResults.payload?.sub, authResults.payload?.jti ? this.sessionCache[authResults.payload.jti] : undefined)) {
                    return next();
                }
                else {
                    throw new HTTPException(403, { message: 'Forbidden' });
                }
            }
            else {
                this.log(authResults.code.toString(), authResults.message);
                return c.text(authResults.message, authResults.code, {
                    'WWW-Authenticate': `Bearer realm="${req.url}",error="HTTP ${authResults.code}",error_description="${authResults.message}"`
                });
            }
        }); // as unknown as MiddlewareHandler
    }
    /**
     * Auth Handler for User Pages
     * @returns MiddlewareHandler
     */
    pageAuth(accessCheckHandler) {
        return (async (c, next) => {
            const req = c.req;
            const redirectURI = this.redirectStyle == 'query' ? `${this.redirectURI}?r=${Buffer.from(req.path).toString('base64')}` : this.redirectURI;
            const authResults = await this.jwtAuth(c);
            if (authResults.code == 200) {
                if (!accessCheckHandler) {
                    return next();
                }
                else if (accessCheckHandler(authResults.payload?.sub, authResults.payload?.jti ? this.sessionCache[authResults.payload?.jti] : undefined)) {
                    return next();
                }
                else {
                    throw new HTTPException(403, { message: 'Forbidden' });
                }
            }
            else {
                this.log(authResults.code.toString(), authResults.message);
                c.header('Referer', req.url);
                return c.redirect(redirectURI);
            }
        }); // as unknown as MiddlewareHandler
    }
    async jwtAuth(c) {
        const jwtCookie = await getSignedCookie(c, typeof this.cookie.secret == 'string' ? this.cookie.secret : this.cookie.secret(c), this.cookie.key, this.cookie.prefix);
        if (jwtCookie) {
            try {
                const payload = await verify(jwtCookie, typeof this.jwt.secret == 'string' ? this.jwt.secret : this.jwt.secret(c), this.jwt.alg);
                if (!this.sessionCache[payload.jti]) {
                    this.log('Cookie not in store');
                    await this.removeSession(c);
                    return { code: 401, message: 'Unauthenticated (old cookie)', payload: undefined };
                }
                this.log('Cookie verified and found in store');
                return { code: 200, message: 'Authenticated', payload };
            }
            catch (err) {
                const errors = [
                    JwtTokenExpired,
                    JwtTokenInvalid,
                    JwtTokenIssuedAt,
                    JwtTokenNotBefore,
                    JwtTokenSignatureMismatched,
                    JwtAlgorithmNotImplemented
                ];
                const error = err;
                if (errors.map(errType => err instanceof errType).includes(true)) {
                    this.log('JWT error:', error.name, error.message);
                    await this.removeSession(c);
                    return { code: 401, message: `JWT error: ${error.name} - ${error.message}`, payload: undefined };
                }
                else {
                    return { code: 500, message: `${error.name} (${error.message}) caused by ${error.cause?.name} (${error.cause?.message})`, payload: undefined };
                }
            }
        }
        else {
            this.log('No cookie found');
            return { code: 401, message: 'Unauthenticated (no cookie)' };
        }
    }
    /**
     * Add Session
         *
     */
    async addSession(c, subject, data) {
        this.log('Checking for pre-existing cookie');
        const jwtCookie = await getSignedCookie(c, typeof this.cookie.secret == 'string' ? this.cookie.secret : this.cookie.secret(c), this.cookie.key, this.cookie.prefix);
        this.log(String(jwtCookie));
        if (jwtCookie) {
            this.log('Pre-existing cookie found, removing');
            await this.removeSession(c);
            this.log('Removed');
        }
        this.log('Setting up variables');
        const now = Math.floor(Number(new Date()) / 1000), newJTI = randomUUID(), jwt = {
            iat: now,
            nbf: now - this.jwt.nbf,
            exp: now + this.sessionLength,
            iss: 'JWTAuth',
            sub: subject,
            aud: this.domain,
            jti: newJTI,
        }, cookie = {
            httpOnly: this.cookie.httpOnly,
            maxAge: this.sessionLength,
            secure: this.cookie.secure,
            prefix: this.cookie.prefix,
            path: this.cookie.path,
            sameSite: this.cookie.sameSite,
        }, jwtSecret = typeof this.jwt.secret == 'string' ? this.jwt.secret : this.jwt.secret(c), cookieSecret = typeof this.cookie.secret == 'string' ? this.cookie.secret : this.cookie.secret(c);
        this.log('Prefix:', cookie.prefix ?? '', (cookie.prefix ?? '').toLowerCase(), String((cookie.prefix ?? '').toLowerCase() != 'host'));
        if (cookie.prefix?.toLowerCase() != 'host') {
            this.log('prefix is not host, setting domain attribute');
            cookie.domain = this.domain;
        }
        this.log('Generating signed cookie');
        await setSignedCookie(c, this.cookie.key, await sign(jwt, jwtSecret, this.jwt.alg), cookieSecret, cookie);
        this.log('Saving session data');
        this.sessionCache[newJTI] = data;
        this.log('Session added');
    }
    /**
     * Get Session
         *
     */
    async getSession(c) {
        try {
            const cookieSecret = typeof this.cookie.secret == 'string' ? this.cookie.secret : this.cookie.secret(c), jwtCookie = await getSignedCookie(c, cookieSecret, this.cookie.key, this.cookie.prefix);
            if (jwtCookie) {
                const jwtSecret = typeof this.jwt.secret == 'string' ? this.jwt.secret : this.jwt.secret(c), payload = await verify(jwtCookie, jwtSecret, this.jwt.alg), jti = decode(jwtCookie).payload.jti;
                return [payload.sub, this.sessionCache[jti]];
            }
        }
        catch (err) {
            this.log(JSON.stringify(err, Object.getOwnPropertyNames(err)));
            return undefined;
        }
    }
    /**
     * Remove Session
         *
     */
    async removeSession(c) {
        const cookieSecret = typeof this.cookie.secret == 'string' ? this.cookie.secret : this.cookie.secret(c), jwtCookie = await getSignedCookie(c, cookieSecret, this.cookie.key, this.cookie.prefix);
        if (jwtCookie) {
            const jti = decode(jwtCookie).payload.jti;
            delete this.sessionCache[jti];
            let key = this.cookie.key;
            if (this.cookie.prefix) {
                key = `__${this.cookie.prefix.charAt(0).toUpperCase()}${this.cookie.prefix.substring(1)}-${key}`;
            }
            deleteCookie(c, key, {
                path: this.cookie.path,
                secure: this.cookie.secure,
                domain: this.cookie.prefix?.toLowerCase() != 'host' ? this.domain : undefined
            });
        }
    }
}
//# sourceMappingURL=index.js.map