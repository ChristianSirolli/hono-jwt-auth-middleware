import { expect, describe, it } from 'vitest';
import { Hono } from 'hono/quick';
import { logger } from 'hono/logger';
import { HTTPException } from 'hono/http-exception';
import { JWTAuth } from 'hono_jwt_auth';
import { pbkdf2Sync } from 'node:crypto';
import { Buffer } from "node:buffer";
function setupEnvironment(jwtConfig) {
    const app = new Hono();
    const sessManager = new JWTAuth(jwtConfig);
    const salt = Uint8Array.from([...Buffer.from('822bd7ab465841f606c350b0810e727a', 'hex').toJSON().data]);
    const users = {
        'John Doe': {
            pwd: '5aa7e76ae1cdfd1959a216062fc5bbb25dea5cb066d9b1f8410ca129bdcd1147dd34b7da77b7ecae14bbd2486a13ab5b50e3f1fd540de1c6644abda1b5a69b0d',
            data: {
                type: 'admin',
                age: 32,
                theme: 'dark'
            }
        },
        'Jane Doe': {
            pwd: '5702754b71e7c97f03ba59bcf3b0f5d14a4a812dd628d1a8bea5307263118f69dad0a2254bcfd8520068084e95625f7b13d6f68fe2c75f6d35f10f984effcd28',
            data: {
                type: 'user',
                age: 29,
                theme: 'light'
            }
        },
    };
    app.use('/*', logger());
    app.onError((err, c) => {
        if (err instanceof HTTPException) {
            if (err.status == 403) {
                return c.text('You are not allowed to access this page.', 403);
            }
            else {
                return err.getResponse();
            }
        }
        else {
            return new HTTPException(500, {
                message: err.name,
                cause: err
            }).getResponse();
        }
    });
    app.get('/', c => c.text('Home'));
    app.post('/api/login', async (c) => {
        try {
            const creds = await c.req.json();
            const hash = pbkdf2Sync(Uint8Array.from([...Buffer.from(creds.pass).toJSON().data]), salt, 100000, 64, 'sha512').toString('hex');
            if (users[creds.user]) {
                const user = users[creds.user];
                if (user.pwd === hash) {
                    await sessManager.addSession(c, creds.user, user.data);
                    return c.text('Okay');
                }
                else {
                    return c.text('Wrong password', 401);
                }
            }
            else {
                return c.text('Unknown User', 401);
            }
        }
        catch (e) {
            console.error(e);
            throw new HTTPException(500, { message: `${e.name} ${e.message}` });
        }
    });
    app.get('/login', sessManager.loginPageHelper(), c => {
        return c.text('Login');
    });
    app.get('/logout', async (c) => {
        await sessManager.removeSession(c);
        return c.redirect('/login');
    });
    app.get('/notpublic', sessManager.pageAuth((_subject, userData) => {
        if (userData.type == 'user' || userData.type == 'admin') {
            return true;
        }
        else {
            return false;
        }
    }), async (c) => {
        const session = (await sessManager.getSession(c));
        return c.text(`Hello ${session[0]}. You're profile type is "${session[1].type}". Your age is ${session[1].age}.`);
    });
    app.get('/admin', sessManager.pageAuth((_subject, userData) => {
        if (userData.type == 'admin') {
            return true;
        }
        else {
            return false;
        }
    }), async (c) => {
        return c.text('Welcome admin ' + (await sessManager.getSession(c))[0]);
    });
    app.get('/api/notpublic', sessManager.apiAuth((_subject, userData) => {
        if (userData.type == 'user' || userData.type == 'admin') {
            return true;
        }
        else {
            return false;
        }
    }), c => {
        return c.text('Okay');
    });
    app.get('/api/admin', sessManager.apiAuth((_subject, userData) => {
        if (userData.type == 'admin') {
            return true;
        }
        else {
            return false;
        }
    }), c => {
        return c.text('Okay');
    });
    return app;
}
function runTests(app) {
    let setCookie;
    it('Should keep guest out of page', async () => {
        // req = new Request('/notpublic', { /* keepalive: true, credentials: 'include' */ });
        const res = await app.request('/notpublic', { /* keepalive: true, credentials: 'include' */});
        expect(res).not.toBeNull();
        expect(res.status).toBe(302); // Nobody is logged in
        const loc = res?.headers?.get('location')?.split('?');
        // const r = new URLSearchParams(loc?.at(1))?.get('r');
        console.log('Headers', [res.headers.get('location'), res.headers.get('referrer'), res.headers.get('referer')], res);
        expect(loc?.at(0)).toBe('/login'); // Nobody is logged in and is redirected to login
        // expect(r != null ? atob(r) : res.headers.get('referer')).not.toBeNull();
        // expect(r != null ? atob(r) : res.headers.get('referer')).toMatch(/(?:https:\/\/localhost)?\/notpublic$/);
    });
    it('Should keep guest out of api', async () => {
        // req = new Request('/api/notpublic', { /* keepalive: true, credentials: 'include' */ });
        const res = await app.request('/api/notpublic', { /* keepalive: true, credentials: 'include' */});
        expect(res).not.toBeNull();
        expect(res.status).toBe(401); // Nobody is logged in
    });
    it('Should not login in Josh Doe', async () => {
        // req = new Request('/api/login', { /* keepalive: true, credentials: 'include', */ method: 'POST', headers: { 'Content-Type': 'application/json' }, body: JSON.stringify({ user: 'Jane Doe', pass: 'applepie' }) });
        const res = await app.request('/api/login', { /* keepalive: true, credentials: 'include', */ method: 'POST', headers: { 'Content-Type': 'application/json' }, body: JSON.stringify({ user: 'Josh Doe', pass: 'admin123' }) });
        expect(res).not.toBeNull();
        expect(res.status).toBe(401); // Josh Doe is not logged in
    });
    it('Should login in Jane Doe', async () => {
        // req = new Request('/api/login', { /* keepalive: true, credentials: 'include', */ method: 'POST', headers: { 'Content-Type': 'application/json' }, body: JSON.stringify({ user: 'Jane Doe', pass: 'applepie' }) });
        const res = await app.request('/api/login', { /* keepalive: true, credentials: 'include', */ method: 'POST', headers: { 'Content-Type': 'application/json' }, body: JSON.stringify({ user: 'Jane Doe', pass: 'applepie' }) });
        console.log(typeof res, res);
        console.log('Cookies and Headers:', [res.headers.get('Authorization'), res.headers.get('Set-Cookie'), res.headers.getSetCookie()]);
        setCookie = res.headers.getSetCookie().at(-1);
        expect(res).not.toBeNull();
        expect(res.status).toBe(200); // Jane Doe is logged in
        expect(await res.text()).toBe('Okay');
    });
    it('Should allow Jane Doe (page)', async () => {
        // req = new Request('/notpublic', { /* keepalive: true, credentials: 'include' */ });
        const res = await app.request('/notpublic', { headers: { Cookie: setCookie } /* keepalive: true, credentials: 'include' */ });
        expect(res).not.toBeNull();
        expect(res.status).toBe(200); // Jane Doe is logged in and allowed
    });
    it('Should forbid Jane Doe (page)', async () => {
        // req = new Request('/admin', { /* keepalive: true, credentials: 'include' */ });
        const res = await app.request('/admin', { headers: { Cookie: setCookie } /* keepalive: true, credentials: 'include' */ });
        expect(res).not.toBeNull();
        expect(res.status).toBe(403); // Jane Doe is logged in and forbidden
    });
    it('Should allow Jane Doe (api)', async () => {
        // req = new Request('/api/notpublic', { /* keepalive: true, credentials: 'include' */ });
        const res = await app.request('/api/notpublic', { headers: { Cookie: setCookie } /* keepalive: true, credentials: 'include' */ });
        expect(res).not.toBeNull();
        expect(res.status).toBe(200); // Jane Doe is logged in and allowed
    });
    it('Should forbid Jane Doe (api)', async () => {
        // req = new Request('/api/admin', { /* keepalive: true, credentials: 'include' */ });
        const res = await app.request('/api/admin', { headers: { Cookie: setCookie } /* keepalive: true, credentials: 'include' */ });
        expect(res).not.toBeNull();
        expect(res.status).toBe(403); // Jane Doe is logged in and forbidden
    });
    it('Should login John Doe', async () => {
        // req = new Request('/api/login', { /* keepalive: true, credentials: 'include', */ method: 'POST', headers: { 'Content-Type': 'application/json' }, body: JSON.stringify({ user: 'John Doe', pass: 'password' }) });
        const res = await app.request('/api/login', { /* keepalive: true, credentials: 'include', */ method: 'POST', headers: { Cookie: setCookie, 'Content-Type': 'application/json' }, body: JSON.stringify({ user: 'John Doe', pass: 'password' }) });
        console.log('Cookies and Headers:', [res.headers.get('Authorization'), res.headers.get('Set-Cookie'), res.headers.getSetCookie()]);
        setCookie = res.headers.getSetCookie().at(-1);
        expect(res).not.toBeNull();
        expect(res.status).toBe(200); // John Doe is logged in
        expect(await res.text()).toBe('Okay');
    });
    it('Should allow John Doe (page)', async () => {
        console.log('setCookie:', setCookie);
        // req = new Request('/admin', { /* keepalive: true, credentials: 'include' */ });
        const res = await app.request('/admin', { headers: { Cookie: setCookie } /* keepalive: true, credentials: 'include' */ });
        expect(res).not.toBeNull();
        expect(res.status).toBe(200); // John Doe is logged in and allowed
    });
    it('Should allow John Doe (api)', async () => {
        // req = new Request('/api/admin', { /* keepalive: true, credentials: 'include' */ });
        const res = await app.request('/api/admin', { headers: { Cookie: setCookie } /* keepalive: true, credentials: 'include' */ });
        expect(res).not.toBeNull();
        expect(res.status).toBe(200); // John Doe is logged in and allowed
    });
    it('Should redirect John Doe to admin', async () => {
        const res = await app.request('/login?r=L2FkbWlu', { referrer: 'https://localhost/admin', headers: { Cookie: setCookie } /* keepalive: true, credentials: 'include' */ });
        expect(res).not.toBeNull();
        expect(res.status).toBe(302); // John Doe is still logged in
        expect(res.headers.get('location')).toMatch(/(?:https:\/\/localhost)?\/admin$/); // John Doe is redirected to admin
    });
    it('Should logout John Doe', async () => {
        // req = new Request('/logout', { /* keepalive: true, credentials: 'include' */ });
        const res = await app.request('/logout', { headers: { Cookie: setCookie } /* keepalive: true, credentials: 'include' */ });
        expect(res).not.toBeNull();
        expect(res.status).toBe(302); // John Doe is logged out
        expect(res.headers.get('location')).toBe('/login'); // John Doe is redirected to login
    });
}
describe('JWT Auth', () => {
    describe('no config (only logging)', () => runTests(setupEnvironment({ logging: true })));
    describe('with domain', () => runTests(setupEnvironment({ logging: true, domain: 'gitlab.com' })));
    describe('with domain and host cookie', () => runTests(setupEnvironment({ logging: true, domain: 'gitlab.com', cookie: { prefix: 'host' } })));
    describe('with redirectStyle = query', () => runTests(setupEnvironment({ logging: true, redirectStyle: 'query' })));
});
//# sourceMappingURL=index.test.js.map