import type { Context, Next } from 'hono';
import type { CookiePrefixOptions } from 'hono/utils/cookie';
export type CookieConfig<HonoContext extends Context = Context> = {
    key?: string;
    secret?: string | ((c: HonoContext) => string);
    httpOnly?: boolean;
    secure?: boolean;
    prefix?: CookiePrefixOptions;
    path?: string;
    sameSite?: 'Strict' | 'Lax' | 'None';
    partitioned?: boolean;
};
export type JWTConfig<HonoContext extends Context = Context> = {
    nbf?: number;
    secret?: string | ((c: HonoContext) => string);
    alg?: "HS512" | "HS256" | "HS384";
};
export type JWTAuthConfig<HonoContext extends Context = Context> = {
    domain?: string;
    sessionLength?: number;
    redirectURI?: string;
    redirectStyle?: 'referrer' | 'query';
    cookie?: CookieConfig<HonoContext>;
    jwt?: JWTConfig<HonoContext>;
    logging?: boolean;
    logger?: (str: string, ...rest: string[]) => void;
};
export declare class JWTAuth<UserData, HonoContext extends Context = Context> {
    domain: string;
    sessionLength: number;
    redirectURI: string;
    redirectStyle: 'referrer' | 'query';
    logging: boolean;
    logger: (str: string | undefined, ...rest: (string | undefined)[]) => void;
    private cookie;
    private jwt;
    private sessionCache;
    /**
     * JWT Auth Session Manager
         *
     */
    constructor(config?: JWTAuthConfig<HonoContext>);
    private log;
    /**
     * Login Page Helper
     * @returns MiddlewareHandler
     */
    loginPageHelper(): (c: HonoContext, next: Next) => Promise<void | (Response & import("hono").TypedResponse<undefined, 302, "redirect">)>;
    /**
     * Auth Handler for API Endpoints
     * @returns MiddlewareHandler
     */
    apiAuth(accessCheckHandler?: (subject?: string, userData?: UserData) => boolean): (c: HonoContext, next: Next) => Promise<void | (Response & import("hono").TypedResponse<string, 100 | 101 | 102 | 103 | -1 | 301 | 302 | 303 | 307 | 308 | 201 | 202 | 203 | 204 | 205 | 206 | 207 | 208 | 226 | 305 | 306 | 300 | 304 | 400 | 401 | 402 | 403 | 404 | 405 | 406 | 407 | 408 | 409 | 410 | 411 | 412 | 413 | 414 | 415 | 416 | 417 | 418 | 421 | 422 | 423 | 424 | 425 | 426 | 428 | 429 | 431 | 451 | 500 | 501 | 502 | 503 | 504 | 505 | 506 | 507 | 508 | 510 | 511, "text">)>;
    /**
     * Auth Handler for User Pages
     * @returns MiddlewareHandler
     */
    pageAuth(accessCheckHandler?: (subject?: string, userData?: UserData) => boolean): (c: HonoContext, next: Next) => Promise<void | (Response & import("hono").TypedResponse<undefined, 302, "redirect">)>;
    private jwtAuth;
    /**
     * Add Session
         *
     */
    addSession(c: HonoContext, subject: string, data: UserData): Promise<void>;
    /**
     * Get Session
         *
     */
    getSession(c: HonoContext): Promise<[string, UserData] | undefined>;
    /**
     * Remove Session
         *
     */
    removeSession(c: HonoContext): Promise<void>;
}
